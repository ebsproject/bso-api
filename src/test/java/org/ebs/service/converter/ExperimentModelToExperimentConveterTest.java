package org.ebs.service.converter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.ebs.model.ExperimentModel;
import org.ebs.model.OccurrenceModel;
import org.ebs.model.repository.OccurrenceRepository;
import org.ebs.service.to.Occurrence;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ExperimentModelToExperimentConveterTest {

    @Mock private OccurrenceModelToOccurrenceConverter occConverter;
    @Mock private OccurrenceRepository occurrenceRepoMock;

    private ExperimentModelToExperimentConverter subject;

    @BeforeEach
    public void init() {
        subject = new ExperimentModelToExperimentConverter(occConverter, occurrenceRepoMock);
    }

    @Test
    public void givenOccurrencesNotNull_whenConvertOccurrences_thenSuccess() {
        when(occConverter.convert(any())).thenReturn(new Occurrence());

        ExperimentModel object = new ExperimentModel();
        object.setOccurrences(Arrays.asList(new OccurrenceModel(), new OccurrenceModel(), new OccurrenceModel()));

        when(occurrenceRepoMock.findByExperimentIdAndDeletedIsFalse(anyInt()))
                .thenReturn(Arrays.asList(new OccurrenceModel(), new OccurrenceModel(), new OccurrenceModel()));
        List<Occurrence> result = subject.convertOccurrences(object);

        assertThat(result).size().isEqualTo(3);
        verify(occConverter, times(3)).convert(any());
    }
    @Test
    public void givenOccurrencesNull_whenConvertOccurrences_thenEmpty() {
        ExperimentModel object = new ExperimentModel();

        List<Occurrence> result = subject.convertOccurrences(object);

        assertThat(result).isEmpty();
    }
}