package org.ebs.service.converter;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.ebs.model.ExperimentDataModel;
import org.ebs.model.ExperimentModel;
import org.ebs.model.OccurrenceModel;
import org.ebs.model.ScaleModel;
import org.ebs.model.VariableModel;
import org.ebs.model.repository.ExperimentDataRepository;
import org.ebs.model.repository.PlotRepository;
import org.ebs.model.repository.VariableRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class OccurrenceModelToOccurrenceConverterTest {

    @Mock private PlotModelToPlotConverter plotConverterMock;
    @Mock private ExperimentDataRepository plotParametersRepoMock;
    @Mock private VariableRepository variableRepoMock;
    @Mock private PlotRepository plotRepoMock;

    private OccurrenceModelToOccurrenceConverter subject;

    @BeforeEach
    public void init() {
        subject = new  OccurrenceModelToOccurrenceConverter(plotConverterMock, plotParametersRepoMock, variableRepoMock, plotRepoMock);
    }

    @Test
    public void givenExperimentDataNotExists_whenGetPlotParameter_thenReturnEmpty() {
        OccurrenceModel object = new OccurrenceModel();
        object.setExperiment(new ExperimentModel());
        when(plotParametersRepoMock.findByExperimentIdAndVariableAbbrevAndDeletedIsFalse(anyInt(), anyString())).thenReturn(Collections.emptyList());

        Optional<String> result = subject.getPlotParameter(object, "abbrev");

        assertThat(result).isEmpty();
    }

    @Test
    public void givenExperimentDataExists_whenGetPlotParameter_thenReturnValue() {
        OccurrenceModel object = new OccurrenceModel();
        object.setExperiment(new ExperimentModel());
        List<ExperimentDataModel> dataMock = asList(new ExperimentDataModel());
        dataMock.get(0).setDataValue("a value");

        when(plotParametersRepoMock.findByExperimentIdAndVariableAbbrevAndDeletedIsFalse(anyInt(), anyString())).thenReturn(dataMock);

        Optional<String> result = subject.getPlotParameter(object, "abbrev");

        assertThat(result).isPresent();
        assertThat(result.get()).isEqualTo("a value");
    }

    @Test
    public void givenVariableExists_whenGetUnit_thenReturnValue() {
        VariableModel variableMock = new VariableModel();
        ScaleModel scale = new ScaleModel();
        scale.setUnit("my unit");
        variableMock.setScale(scale);

        when(variableRepoMock.findByAbbrevAndDeletedIsFalse(anyString())).thenReturn(asList(variableMock));

        String result = subject.getUnit("abbrev");

        assertNotNull(result);
        assertThat(result).isEqualTo("my unit");
    }

    @Test
    public void givenVariableNotExists_whenGetUnit_thenReturnEmpty() {

        when(variableRepoMock.findByAbbrevAndDeletedIsFalse(anyString())).thenReturn(emptyList());

        String result = subject.getUnit("abbrev");

        assertNull(result);
    }

    @Test
    public void givenScaleNotExists_whenGetUnit_thenReturnValue() {

        when(variableRepoMock.findByAbbrevAndDeletedIsFalse(anyString())).thenReturn(asList(new VariableModel()));

        String result = subject.getUnit("abbrev");

        assertNull(result);
    }

}