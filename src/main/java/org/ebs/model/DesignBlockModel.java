package org.ebs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "experiment_design", schema = "experiment")
@Getter
@Setter
@ToString
public class DesignBlockModel extends Auditable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "block_type")
    private String blockType;

    @Column(name = "block_name")
    private String blockName;

    @Column(name = "block_value")
    private String blockValue;

    @Column(name = "block_level_number")
    private Integer blockLevelNumber;

    @JoinColumn(name = "plot_id")
    @ManyToOne
    private PlotModel plot;

    @ToString.Exclude
    @Column( name= "is_void", updatable = false, insertable = false)
    private Boolean isVoid;

}