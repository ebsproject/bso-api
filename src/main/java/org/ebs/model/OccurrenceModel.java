package org.ebs.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "occurrence", schema = "experiment")
@Getter
@Setter
@ToString
public class OccurrenceModel extends Auditable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "occurrence_code")
    private String code;

    @Column(name = "occurrence_name")
    private String name;

    @Column(name = "occurrence_status")
    private String status;

    @Column
    private String description;

    @Column(name = "occurrence_number")
    private int number;

    @Column(name = "geospatial_object_id")
    private Integer geospatialObjectId;

    @Column
    private String notes;

    @Column(name = "rep_count")
    private Integer repCount;

    @Column(name = "site_id")
    private Integer siteId;

    @Column(name = "field_id")
    private Integer fieldId;

    @ManyToOne
    @JoinColumn(name = "experiment_id")
    @ToString.Exclude
    private ExperimentModel experiment;

    @OneToMany(mappedBy = "occurrence")
    @ToString.Exclude
    private List<PlotModel> plots;

    @Column(name = "experiment_id", insertable = false, updatable = false)
    private Integer experimentId;

    @ManyToOne
    @JoinColumn(name = "id", referencedColumnName = "occurrence_id", insertable = false, updatable = false)
    @ToString.Exclude
    private LocationOccurrenceGroupModel locationOccurrenceGroup;
}