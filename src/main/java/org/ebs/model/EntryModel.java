package org.ebs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table( name = "entry", schema = "experiment")
@Getter @Setter @ToString
public class EntryModel extends Auditable{

   private static final long serialVersionUID = 1L;

   @Id
   @Column
   @GeneratedValue( strategy = GenerationType.IDENTITY)
   private Integer id;

   @Column(name = "entry_code")
   private String code;
   
   @Column(name = "entry_number")
   private Integer number;
   
   @Column(name = "entry_name")
   private String name;
   
   @Column(name = "entry_type")
   private String type;
   
   @Column(name = "entry_role")
   private String role;
   
   @ManyToOne
   @JoinColumn(name = "entry_list_id")
   @ToString.Exclude
   private EntryListModel entryList;
   
   @ManyToOne
   @JoinColumn(name = "germplasm_id")
   @ToString.Exclude
   private GermplasmModel germplasm;
   
   @Column(name = "seed_id")
   private Integer seedId;
}