package org.ebs.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table( name = "experiment", schema = "experiment")
@Getter @Setter @ToString
public class ExperimentModel extends Auditable{

   private static final long serialVersionUID = 1L;

   @Id
   @Column
   @GeneratedValue( strategy = GenerationType.IDENTITY)
   private int id;

   @Column(name = "experiment_year") private int year;
   @Column(name = "planting_season") private String plantingSeason;
   @Column(name = "experiment_code") private String code;
   @Column(name = "experiment_name") private String name;
   @Column(name = "experiment_objective") private String objective;
   @Column(name = "experiment_type") private String type;
   @Column(name = "experiment_sub_type") private String subType;
   @Column(name = "experiment_sub_sub_type") private String subSubType;
   @Column(name = "experiment_design_type") private String designType;
   @Column(name = "experiment_status") private String status;
   @Column private String description;
   @Column private String notes;
   @Column(name = "program_id") private int programId;
   @Column(name = "pipeline_id") private Integer pipelineId;

   @ManyToOne
   @JoinColumn(name = "stage_id", nullable = false)
   private StageModel stage;

   @Column(name = "project_id") private Integer projectId;
   
   @Column(name = "season_id")
   private int seasonId;

   @Column(name = "crop_id") private int cropId;
   @Column(name = "steward_id") private int stewardId;
   @Column(name = "experiment_plan_id") private Integer planId;
   @Column(name = "data_process_id") private Integer dataProcessId;

   @OneToMany(mappedBy = "experiment")
   @ToString.Exclude
   private List<OccurrenceModel> occurrences;
}