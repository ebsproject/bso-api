package org.ebs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table( name = "program", schema = "tenant")
@Getter @Setter @ToString
public class ProgramModel extends Auditable{

   private static final long serialVersionUID = 1L;

   @Id
   @Column
   @GeneratedValue( strategy = GenerationType.IDENTITY)
   private Integer id;

   @Column(name = "program_code")
   private String code;
   
   @Column(name = "program_name")
   private String name;
   
   @Column
   private String description;

}