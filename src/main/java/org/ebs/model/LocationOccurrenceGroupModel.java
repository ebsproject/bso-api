package org.ebs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "location_occurrence_group", schema = "experiment")
@Getter
@Setter
@ToString
public class LocationOccurrenceGroupModel extends Auditable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "occurrence_id")
    private Integer occurrenceId;

    @ManyToOne
    @JoinColumn(name = "location_id")
    @ToString.Exclude
    private LocationModel location;

}