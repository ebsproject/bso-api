package org.ebs.model.repository;

import java.util.Set;
import java.util.Optional;

import org.ebs.model.PersonModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<PersonModel, Integer>,
   RepositoryExt<PersonModel>{

      Optional<PersonModel> findByIdAndDeletedIsFalse(int personId);
      Set<PersonModel> findByIdInAndDeletedIsFalse(Set<Integer> personIds);


}