package org.ebs.model.repository;

import java.util.List;

import org.ebs.model.ScaleModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ScaleRepository extends JpaRepository<ScaleModel, Integer>, RepositoryExt<ScaleModel> {

    List<ScaleModel> findByIdAndDeletedIsFalse(Integer id);

}