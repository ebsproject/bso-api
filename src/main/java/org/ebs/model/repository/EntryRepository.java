package org.ebs.model.repository;

import java.util.List;

import org.ebs.model.EntryModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EntryRepository extends JpaRepository<EntryModel, Integer>,
    RepositoryExt<EntryModel>{

        List<EntryModel> findByEntryListIdAndDeletedIsFalse(int entryListId);
}