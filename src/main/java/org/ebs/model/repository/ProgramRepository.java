package org.ebs.model.repository;

import java.util.Set;
import java.util.Optional;

import org.ebs.model.ProgramModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProgramRepository extends JpaRepository<ProgramModel, Integer>,
   RepositoryExt<ProgramModel>{

      Optional<ProgramModel> findByIdAndDeletedIsFalse(int programId);
      Set<ProgramModel> findByIdInAndDeletedIsFalse(Set<Integer> programIds);


}