package org.ebs.model.repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.ebs.model.GeospatialObjectModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GeospatialObjectRepository extends JpaRepository<GeospatialObjectModel, Integer>,
   RepositoryExt<GeospatialObjectModel>{

      Optional<GeospatialObjectModel> findByIdAndDeletedIsFalse(int geospatialObjectDbId);
      Set<GeospatialObjectModel> findByIdInAndDeletedIsFalse(Set<Integer> geospatialObjectDbIds);
      List<GeospatialObjectModel> findByIdAndParentIdIsNotNullAndTypeAndDeletedIsFalse(Integer geospatialObjectDbId, String type);
}