package org.ebs.model.repository;

import java.util.List;

import org.ebs.model.PlotModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlotRepository extends JpaRepository<PlotModel, Integer>,
   RepositoryExt<PlotModel>{

      List<PlotModel> findByOccurrenceIdAndDeletedIsFalse(int occurrenceId);
}