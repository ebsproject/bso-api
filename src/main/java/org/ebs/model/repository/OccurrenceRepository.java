package org.ebs.model.repository;

import java.util.List;

import org.ebs.model.OccurrenceModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OccurrenceRepository extends JpaRepository<OccurrenceModel, Integer>,
    RepositoryExt<OccurrenceModel>{

        List<OccurrenceModel> findByExperimentIdAndDeletedIsFalse(int experimentId);
}