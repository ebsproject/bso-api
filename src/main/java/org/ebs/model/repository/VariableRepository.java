package org.ebs.model.repository;

import java.util.List;

import org.ebs.model.VariableModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VariableRepository extends JpaRepository<VariableModel, Integer>, RepositoryExt<VariableModel> {

    List<VariableModel> findByAbbrevAndDeletedIsFalse(String abbrev);

}