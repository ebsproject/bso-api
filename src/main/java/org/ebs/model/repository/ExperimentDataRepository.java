package org.ebs.model.repository;

import java.util.List;

import org.ebs.model.ExperimentDataModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExperimentDataRepository
            extends JpaRepository<ExperimentDataModel, Integer>, RepositoryExt<ExperimentDataModel> {

      List<ExperimentDataModel> findByExperimentIdAndVariableIdAndDeletedIsFalse(int experimentId,
            int variableId);

      List<ExperimentDataModel> findByExperimentIdAndVariableAbbrevAndDeletedIsFalse(int experimentId,
            String variableAbbrev);
}