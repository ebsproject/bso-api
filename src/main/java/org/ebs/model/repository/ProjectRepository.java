package org.ebs.model.repository;

import java.util.Optional;
import java.util.Set;

import org.ebs.model.ProjectModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectRepository extends JpaRepository<ProjectModel, Integer>,
   RepositoryExt<ProjectModel>{

      Optional<ProjectModel> findByIdAndDeletedIsFalse(int projectId);
      Set<ProjectModel> findByIdInAndDeletedIsFalse(Set<Integer> projectIds);


}