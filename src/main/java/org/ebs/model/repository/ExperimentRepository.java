package org.ebs.model.repository;

import java.util.Optional;

import org.ebs.model.ExperimentModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExperimentRepository extends JpaRepository<ExperimentModel, Integer>,
   RepositoryExt<ExperimentModel>{

      Optional<ExperimentModel> findByIdAndDeletedIsFalse(int elementId);

}