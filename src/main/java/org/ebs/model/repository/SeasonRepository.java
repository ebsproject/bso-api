
package org.ebs.model.repository;

import java.util.Set;
import java.util.Optional;

import org.ebs.model.SeasonModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SeasonRepository extends JpaRepository<SeasonModel, Integer>,
   RepositoryExt<SeasonModel>{

      Optional<SeasonModel> findByIdAndDeletedIsFalse(int seasonId);
      Set<SeasonModel> findByIdInAndDeletedIsFalse(Set<Integer> seasonIds);


}