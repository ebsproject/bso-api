package org.ebs.model.repository;

import java.util.Optional;
import java.util.Set;

import org.ebs.model.LocationOccurrenceGroupModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LocationOccurrenceGroupRepository
      extends JpaRepository<LocationOccurrenceGroupModel, Integer>, RepositoryExt<LocationOccurrenceGroupModel> {

   Optional<LocationOccurrenceGroupModel> findByOccurrenceIdAndDeletedIsFalse(int occurrenceId);
   Set<LocationOccurrenceGroupModel> findByOccurrenceIdInAndDeletedIsFalse(Set<Integer> occurrenceIds);
}