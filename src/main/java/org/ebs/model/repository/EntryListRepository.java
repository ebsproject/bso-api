package org.ebs.model.repository;

import java.util.List;

import org.ebs.model.EntryListModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EntryListRepository extends JpaRepository<EntryListModel, Integer>,
   RepositoryExt<EntryListModel>{

      List<EntryListModel> findByExperimentIdAndDeletedIsFalse(int experimentId);
}