package org.ebs.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "plot", schema = "experiment")
@Getter
@Setter
@ToString
public class PlotModel extends Auditable {

   private static final long serialVersionUID = 1L;

   @Id
   @Column
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private Integer id;

   @ToString.Exclude
   @ManyToOne
   @JoinColumn(name = "occurrence_id")
   private OccurrenceModel occurrence;
   
   @Column(name = "location_id")
   private Integer locationId;
   
   @ToString.Exclude
   @ManyToOne
   @JoinColumn(name = "entry_id")
   private EntryModel entry;

   @Column(name = "plot_code")
   private String code;

   @Column(name = "plot_number")
   private Integer number;

   @Column(name = "plot_type")
   private String type;

   @Column(name = "rep")
   private Integer rep;

   @Column(name = "design_x")
   private Integer designX;

   @Column(name = "design_y")
   private Integer designY;

   @Column(name = "plot_order_number")
   private Integer order;

   @Column(name = "pa_x")
   private Integer paX;

   @Column(name = "pa_y")
   private Integer paY;

   @Column(name = "field_x")
   private Integer fieldX;

   @Column(name = "field_y")
   private Integer fieldY;

   @Column(name = "plot_status")
   private String status;

   @Column(name = "plot_qc_code")
   private String qcCode;

   @Column(name = "block_number")
   private String block;

   @Column(name = "harvest_status")
   private String harvestStatus;

   @ToString.Exclude
   @OneToMany(mappedBy = "plot")
   private List<DesignBlockModel> designBlocks;

}