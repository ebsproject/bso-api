package org.ebs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "location", schema = "experiment")
@Getter
@Setter
@ToString
public class LocationModel extends Auditable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "location_code")
    private String locationCode;

    @Column(name = "location_name")
    private String locationName;

    @Column(name = "location_status")
    private String locationStatus;

    @Column(name = "location_type")
    private String locationType;

    @Column(name = "steward_id")
    private Integer locationStewardId;

    @Column(name = "geospatial_object_id")
    private Integer geoSpatialObjectId;

    @Column(name = "location_number")
    private Integer locationNumber;
}
