package org.ebs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "experiment_data", schema = "experiment")
@Getter
@Setter
@ToString
public class ExperimentDataModel extends Auditable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "experiment_id")
    private Integer experimentId;

    @Column(name = "data_value")
    private String dataValue;

    @Column(name = "data_qc_code")
    private String dataQcCode;

    @ManyToOne
    @JoinColumn(name = "variable_id", referencedColumnName="id", nullable = false, insertable =false, updatable =false)
    @ToString.Exclude
    private VariableModel variable;


}