package org.ebs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table( name = "geospatial_object", schema = "place")
@Getter @Setter @ToString
public class ParentGeospatialObjectModel extends Auditable{

   private static final long serialVersionUID = 1L;

   @Id
   @Column
   @GeneratedValue( strategy = GenerationType.IDENTITY)
   private Integer id;

   @Column(name = "geospatial_object_code")
   private String code;

   @Column(name = "geospatial_object_name")
   private String name;

   @Column(name = "geospatial_object_type")
   private String type;

   @Column(name = "geospatial_object_subtype")
   private String subtype;

   @Column(name = "geospatial_coordinates")
   private String coordinates;

   @Column(name = "altitude")
   private String altitude;

   @Column(name = "description")
   private String description;

   @Column(name = "parent_geospatial_object_id")
   private Integer parentId;

   @Column(name = "root_geospatial_object_id")
   private Integer rootId;
}