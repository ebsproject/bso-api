package org.ebs.model;

import static javax.persistence.CascadeType.ALL;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "entry_list", schema = "experiment")
@Getter
@Setter
@ToString
public class EntryListModel extends Auditable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "entry_list_code")
    private String code;

    @Column(name = "entry_list_name")
    private String name;

    @Column
    private String description;

    @Column(name = "entry_list_status")
    private String status;

    @Column(name = "experiment_id")
    private Integer experimentId;

    @Column(name = "entry_list_type")
    private String type;

    @OneToMany(mappedBy = "entryList", cascade = ALL)
    @ToString.Exclude
    private List<EntryModel> entries;

}