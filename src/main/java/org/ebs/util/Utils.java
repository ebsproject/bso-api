package org.ebs.util;

import java.beans.FeatureDescriptor;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

public class Utils {

    private static final String GEOJSON_TEMPLATE = "{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"{geometry}\",\"coordinates\":{coordinates}}}]}";

    private static String[] getNullPropertyNames(Object source) {
        final BeanWrapper wrappedSource = new BeanWrapperImpl(source);
        return Stream.of(wrappedSource.getPropertyDescriptors())
                .map(FeatureDescriptor::getName)
                .filter(propertyName -> wrappedSource.getPropertyValue(propertyName) == null)
                .toArray(String[]::new);
    }

    /**
     * Copy properties between beans which are not null values in the source
     * 
     * @param source object to be copied
     * @param target object receiving the copied values
     */
    public static void copyNotNulls(Object source, Object target) {
        BeanUtils.copyProperties(source, target, getNullPropertyNames(source));
    }

    /**
     * Returns a valid geojson object with the following structure: 1 Feature
     * Collection, with one Feature and one Geometry. The geometry type is set based
     * on the number of coordinates provided
     * 
     * @param coordinates in a postgreSQL notation. Example:
     *                    "((33.9447710680755,-9.55896224165781))"
     * @return the geojson representation of the coordinates
     */
    public static String rawGeojsonFromCoordinates(String coordinates) {

        Pattern pattern = Pattern.compile("\\([+-]?([0-9]*[.])?[0-9]+(?:,\\s*[+-]?([0-9]*[.])?[0-9]+)+\\)");
        List<String> list = new ArrayList<String>();
        
        // Matching the compiled pattern in the String
        Matcher matcher = pattern.matcher(coordinates);
        while (matcher.find()) {
            String formattedCoordinates = matcher.group().replaceAll("\\(", "[").replaceAll("\\)", "]");
            list.add(formattedCoordinates);
        }
        coordinates = null;
        String geometricType = null;

        if (list.size() > 3) {
            coordinates = String.join(",", list);
            coordinates = "[[" + coordinates + "]]";
            geometricType = "Polygon";
        } else if (list.size() == 1) {
            coordinates = String.join(",", list);
            coordinates = coordinates.replaceAll("\\(", "[").replaceAll("\\)", "]");
            geometricType = "Point";
        }

        return GEOJSON_TEMPLATE.replace("{coordinates}", coordinates).replace("{geometry}", geometricType);
    }
}