package org.ebs.util;

import java.util.HashMap;
import java.util.Map;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * Clears data in request cache, if any
 * 
 */
@Aspect
@Order(10)
@Component
public class RequestCacheAdvice {

	public static final ThreadLocal<Map<String,Object>> REQUEST_CACHE =
        ThreadLocal.withInitial(() -> new HashMap<>());

    @Pointcut("@within(org.springframework.web.bind.annotation.RestController)")
    private void allRestController() { }

    @Before("allRestController()")
    void clearCache(JoinPoint joinPoint) {
        REQUEST_CACHE.get().clear();
    }


}