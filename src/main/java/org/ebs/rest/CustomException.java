package org.ebs.rest;

public class CustomException extends RuntimeException {
  private static final long serialVersionUID = 1L;

  public CustomException(String m) {
    super(m);
  }
}