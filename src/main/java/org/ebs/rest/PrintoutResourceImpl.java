package org.ebs.rest;

import org.ebs.service.PrintoutService;
import org.ebs.service.to.PlotInfo;
import org.ebs.util.brapi.BrPagedResponse;
import org.ebs.util.brapi.BrapiResponseBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
class PrintoutResourceImpl implements PrintoutResource {

    private final PrintoutService printoutService;

    @Override
    @GetMapping("printout-plots/{occurrenceId}")
    public BrPagedResponse<PlotInfo> findPlotInfo(@PathVariable int occurrenceId) {
        return BrapiResponseBuilder.forData(printoutService.findPlotInfo(occurrenceId))
            .withStatusSuccess()
            .build();
    }

}