package org.ebs.rest;

import java.util.Set;

import org.ebs.service.to.PlotInfo;
import org.ebs.util.brapi.BrPagedResponse;
import org.springframework.validation.annotation.Validated;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;

@Validated
@SecurityRequirement(name = "Bearer Token")
public interface PrintoutResource {

    @Operation(description = "Returns plot information from a given occurrence")
    BrPagedResponse<PlotInfo> findPlotInfo(
            @Parameter(description = "Occurrence ID to search for") int occurrenceId);

}