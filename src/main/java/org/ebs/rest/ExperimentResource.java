package org.ebs.rest;

import java.util.Set;

import org.ebs.service.to.ExperimentExportFile;
import org.springframework.validation.annotation.Validated;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;

@Validated
@SecurityRequirement(name = "Bearer Token")
public interface ExperimentResource {

    @Operation(description = "Returns the information for all experiment requested as well as their internal definitions")
    ExperimentExportFile findExperiments(
            @Parameter(description = "List of experiment IDs to search for") Set<Integer> experimentId);

}