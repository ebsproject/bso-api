package org.ebs.rest;

import java.util.Set;

import org.ebs.service.ExperimentService;
import org.ebs.service.to.ExperimentExportFile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
class ExperimentDesignArraysResourceImpl implements ExperimentResource {

    private final ExperimentService experimentService;

    @Override
    @GetMapping(path = "experiment-design-arrays")
    public ExperimentExportFile findExperiments(
            @RequestParam(value = "experimentId", required = true) Set<Integer> experimentId) {
        return experimentService.generateExportDefinition(experimentId);
    }

}