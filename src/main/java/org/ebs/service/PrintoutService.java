package org.ebs.service;

import org.ebs.service.to.PlotInfo;
import org.springframework.data.domain.Page;

public interface PrintoutService {

    /**
     * Finds plot and its associated information from an occurrence
     * @param occurrenceId
     * @return the page with plots from a given occurrence
     */
    Page<PlotInfo> findPlotInfo (int occurrenceId);

}