package org.ebs.service;

import java.util.Set;

import org.ebs.service.to.Experiment;
import org.ebs.service.to.ExperimentExportFile;

public interface ExperimentService {

    ExperimentExportFile generateExportDefinition(Set<Integer> experimentIds);
    Experiment findExperiment (int experimentId);

}