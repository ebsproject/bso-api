package org.ebs.service;

import static java.util.stream.Collectors.toSet;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.ebs.model.ExperimentModel;
import org.ebs.model.LocationOccurrenceGroupModel;
import org.ebs.model.OccurrenceModel;
import org.ebs.model.repository.EntryListRepository;
import org.ebs.model.repository.EntryRepository;
import org.ebs.model.repository.ExperimentRepository;
import org.ebs.model.repository.PersonRepository;
import org.ebs.model.repository.ProgramRepository;
import org.ebs.model.repository.ProjectRepository;
import org.ebs.model.repository.SeasonRepository;
import org.ebs.model.repository.GeospatialObjectRepository;
import org.ebs.model.repository.LocationOccurrenceGroupRepository;
import org.ebs.model.repository.OccurrenceRepository;
import org.ebs.service.to.Entry;
import org.ebs.service.to.Experiment;
import org.ebs.service.to.ExperimentExportFile;
import org.ebs.service.to.Person;
import org.ebs.service.to.Program;
import org.ebs.service.to.Project;
import org.ebs.service.to.Season;
import org.ebs.service.to.GeospatialObject;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
class ExperimentDesignArraysServiceImpl implements ExperimentService {

    private final ConversionService converter;
    private final ExperimentRepository experimentRepo;
    private final EntryListRepository entryListRepo;
    private final EntryRepository entryRepo;
    private final PersonRepository personRepo;
    private final ProgramRepository programRepo;
    private final ProjectRepository projectRepo;
    private final SeasonRepository seasonRepo;
    private final GeospatialObjectRepository geospatialObjectRepo;
    private final LocationOccurrenceGroupRepository locationOccurrenceRepo;
    private final OccurrenceRepository occurrenceRepo;

    @Override
    public ExperimentExportFile generateExportDefinition(Set<Integer> experimentIds) {
        ExperimentExportFile export = new ExperimentExportFile();
        List<Experiment> experiments = experimentIds.stream().map(id -> findExperiment(id)).collect(toList());
        experiments = experiments.stream().filter(t -> t.getId() != 0).collect(Collectors.toList());
        export.setExperiments(experiments);
        export.getReferences().putIfAbsent("persons", collectPersonReferences(export));
        export.getReferences().putIfAbsent("programs", collectProgramReferences(export));
        export.getReferences().putIfAbsent("projects", collectProjectReferences(export));
        export.getReferences().putIfAbsent("seasons", collectSeasonReferences(export));
        export.getReferences().putIfAbsent("geospatialObjects", collectGeospatialObjectReferences(export));

        return export;
    }

    Set<Object> collectPersonReferences(ExperimentExportFile export) {
        Set<Integer> personIds = export.getExperiments().stream().map(e -> e.getStewardId()).collect(toSet());

        return personRepo.findByIdInAndDeletedIsFalse(personIds).stream().map(p -> converter.convert(p, Person.class))
                .collect(toSet());

    }

    Set<Object> collectProgramReferences(ExperimentExportFile export) {
        Set<Integer> programIds = export.getExperiments().stream().map(e -> e.getProgramId()).collect(toSet());

        return programRepo.findByIdInAndDeletedIsFalse(programIds).stream()
                .map(p -> converter.convert(p, Program.class)).collect(toSet());

    }

    Set<Object> collectProjectReferences(ExperimentExportFile export) {
        Set<Integer> projectIds = export.getExperiments().stream().map(e -> e.getProjectId()).collect(toSet());

        return projectRepo.findByIdInAndDeletedIsFalse(projectIds).stream()
                .map(p -> converter.convert(p, Project.class)).collect(toSet());
    }

    @Override
    public Experiment findExperiment(int experimentId) {
        Optional<ExperimentModel> experimentModel = experimentRepo.findByIdAndDeletedIsFalse(experimentId);
        Experiment experiment;
        if (experimentModel.equals(Optional.empty())) {
            experimentModel = experimentRepo.findByIdAndDeletedIsFalse(0);
            experiment = new Experiment();
        } else {
            experiment = converter.convert(experimentModel.orElse(null), Experiment.class);
            experiment.setEntryList(retrieveEntries(experimentId));
        }

        return experiment;
    }

    List<Entry> retrieveEntries(int experimentId) {
        int entryListId = entryListRepo.findByExperimentIdAndDeletedIsFalse(experimentId).stream().findFirst()
                .map(d -> d.getId()).orElse(null);

        return entryRepo.findByEntryListIdAndDeletedIsFalse(entryListId).stream()
                .map(p -> converter.convert(p, Entry.class)).collect(toList());
    }

    Set<Object> collectSeasonReferences(ExperimentExportFile export) {
        Set<Integer> seasonIds = export.getExperiments().stream().map(e -> e.getSeasonId()).collect(toSet());

        return seasonRepo.findByIdInAndDeletedIsFalse(seasonIds).stream().map(p -> converter.convert(p, Season.class))
                .collect(toSet());
    }

    Set<Object> collectGeospatialObjectReferences(ExperimentExportFile export) {
        Set<Integer> geospatialObjectDbIds = new HashSet<Integer>();

        Set<Integer> occurrenceIds = export.getExperiments().stream().flatMap(e -> e.getOccurrences().stream())
                .map(o -> o.getId()).collect(toSet());

        for (Integer i : occurrenceIds) {

            // get site_id
            Optional<OccurrenceModel> occurrence = occurrenceRepo.findById(i);
            Integer siteId = occurrence.map(p -> p.getSiteId()).orElse(0);
            Integer fieldId = occurrence.map(p -> p.getFieldId()).orElse(0);

            geospatialObjectDbIds.add(siteId);

            // check if the field ID has valid site
            Integer validSiteId = geospatialObjectRepo
                    .findByIdAndParentIdIsNotNullAndTypeAndDeletedIsFalse(fieldId, "field").stream()
                    .filter(d -> d.getParent().getType().equals("site")).findFirst().map(d -> d.getParent().getId())
                    .orElse(0);

            if (validSiteId.equals(siteId)) {
                geospatialObjectDbIds.add(fieldId);
            }

            // check if planting area ID has valid hierarchy
            Integer plantingAreaId = locationOccurrenceRepo.findByOccurrenceIdAndDeletedIsFalse(i)
                    .map(d -> d.getLocation().getGeoSpatialObjectId()).orElse(0);

            /**
             * validate planting area ID Planting Area ID is valid if the
             * parent_geospatial_object_id is not null and the parent type is field
             */
            Integer validFieldId = geospatialObjectRepo
                    .findByIdAndParentIdIsNotNullAndTypeAndDeletedIsFalse(plantingAreaId, "planting area").stream()
                    .filter(d -> d.getParent().getType().equals("field")).findFirst().map(d -> d.getParent().getId())
                    .orElse(0);

            if (validFieldId.equals(fieldId)) {

                /**
                 * validate field ID Field is valid if the parent_geospatial_object_id is not
                 * null and the parent type is site
                 */
                validSiteId = geospatialObjectRepo
                        .findByIdAndParentIdIsNotNullAndTypeAndDeletedIsFalse(fieldId, "field").stream()
                        .filter(d -> d.getParent().getType().equals("site")).findFirst().map(d -> d.getParent().getId())
                        .orElse(0);

                if (validSiteId.equals(siteId)) {

                    geospatialObjectDbIds.add(plantingAreaId);
                }
            }
        }

        geospatialObjectDbIds.remove(null);

        return geospatialObjectRepo.findByIdInAndDeletedIsFalse(geospatialObjectDbIds).stream()
                .map(p -> converter.convert(p, GeospatialObject.class)).collect(toSet());
    }
}