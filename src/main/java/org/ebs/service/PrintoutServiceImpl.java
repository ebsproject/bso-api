package org.ebs.service;

import static java.util.stream.Collectors.toList;

import java.util.List;

import org.ebs.model.PlotModel;
import org.ebs.model.repository.PlotRepository;
import org.ebs.service.to.PlotInfo;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
class PrintoutServiceImpl implements PrintoutService {

    private final PlotRepository plotRepository;
    private final ConversionService converter;

    @Override
    public Page<PlotInfo> findPlotInfo(int occurrenceId) {
        List<PlotModel> plots = plotRepository.findByOccurrenceIdAndDeletedIsFalse(occurrenceId);
        
        return new PageImpl<>(plots.stream()
            .map(p -> converter.convert(p, PlotInfo.class))
            .collect(toList()));
    }

}