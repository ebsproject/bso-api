package org.ebs.service.to;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
@NoArgsConstructor @AllArgsConstructor
public class Project {

    @JsonProperty("projectId")
    private Integer id;
    
    @JsonProperty("projectCode")
    private String code;
    
    @JsonProperty("projectName")
    private String name;

}