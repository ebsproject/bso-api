package org.ebs.service.to;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor @NoArgsConstructor
public class PlotInfo {
    private int qrCode;
    private String plotCode;
    private String entryCode;
    private String program;
    private String occurrenceName;
    private Integer plotNumber;
    private int croppingYear;
    private String croppingSeason;
    private Integer entryNumber;
    private String germplasmName;
    private String parentage;
    private String locationName;
}
