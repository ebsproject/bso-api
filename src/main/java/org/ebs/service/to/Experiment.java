package org.ebs.service.to;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
@NoArgsConstructor @AllArgsConstructor
public class Experiment {
   @JsonProperty("experimentId")
   private int id;

   @JsonProperty("experimentName")
   private String name;

   @JsonProperty("experimentCode")
   private String code;

   @JsonProperty("experimentStewardId")
   private int stewardId;

   private int programId;

   private Integer projectId;

   @JsonProperty("statDesignType")
   private String designType;

   @JsonProperty("experimentType")
   private String type;

   @JsonProperty("experimentSubType")
   private String subType;

   @JsonProperty("experimentSubSubType")
   private String subSubType;

   private int seasonId;

   private int year;

   private String stage;

   private String plantingSeason;

   @ToString.Exclude
   private List<Occurrence> occurrences;

   @ToString.Exclude
   private List<Entry> entryList;

   // TODO map it, future sprint
   @ToString.Exclude
   private List<String> traitList;
}