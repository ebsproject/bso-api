package org.ebs.service.to;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
@NoArgsConstructor @AllArgsConstructor
public class Trait {

    @JsonProperty("traitId")
    private Integer id;
    
    @JsonProperty("traitName")
    private String name;
    
    @JsonProperty("traiCode")
    private String code;
}