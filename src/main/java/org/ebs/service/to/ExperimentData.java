package org.ebs.service.to;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
@NoArgsConstructor @AllArgsConstructor
public class ExperimentData {
  
  private Integer experimentId;

  private String dataValue;

  private String dataQcCode;

  // private String variableName;
}
