package org.ebs.service.to;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
@NoArgsConstructor @AllArgsConstructor
public class ExperimentExportFile {

    private List<Experiment> experiments;
    private List<LocationPlot> locationPlots;
    private Map<String,Set<Object>> references = new HashMap<>();
}