package org.ebs.service.to;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
@NoArgsConstructor @AllArgsConstructor
public class DesignBlock {
    private String blockType;
    private String blockName;
    private String blockValue;
    private Integer blockLevelNumber;

 }