package org.ebs.service.to;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
@NoArgsConstructor @AllArgsConstructor
public class Program {

    @JsonProperty("programId")
    private Integer id;
    
    @JsonProperty("programCode")
    private String code;
    
    @JsonProperty("programName")
    private String name;
    
    private String description;

}