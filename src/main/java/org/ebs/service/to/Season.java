package org.ebs.service.to;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor @AllArgsConstructor
public class Season {

   @JsonProperty("seasonId")
   private Integer id;
   
   @JsonProperty("seasonCode")
   private String seasonCode;
   
   @JsonProperty("seasonName")
   private String seasonName;
}