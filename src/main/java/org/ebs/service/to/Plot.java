package org.ebs.service.to;

import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
@NoArgsConstructor @AllArgsConstructor
public class Plot {

    @JsonProperty("plotId")
    private Integer id;

    private Integer entryId;

    @JsonProperty("plotNumber")
    private Integer number;
    private Integer rep;

    private String block;

    private List<DesignBlock> designBlocks;

    private Integer designX;

    private Integer designY;

    private Integer paX;

    private Integer paY;

    private Integer fieldX;

    private Integer fieldY;

    //TODO map it
    private String barcode;

 }