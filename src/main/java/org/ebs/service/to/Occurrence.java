package org.ebs.service.to;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
@NoArgsConstructor @AllArgsConstructor
public class Occurrence {

    @JsonProperty("occurrenceId")
    private Integer id;

    //TODO map it, future sprint
    private Integer experimentGroupId;
    //TODO map it, future sprint
    private String experimentGroupName;

    @JsonProperty("occurrenceName")
    private String name;

    @JsonProperty("occurrenceRemarks")
    private String remarks;

    private Integer siteId;
    private Integer fieldId;

    @JsonProperty("locationId")
    private Integer locationId;

    private Integer plantingAreaId;

    //TODO map it, future sprint
    @JsonProperty("occurrenceManagementId")
    private Integer managementId;

    //TODO map it
    private String plotTypeName;

    //TODO map it, future sprint
    private String plantingDate;

    @ToString.Exclude
    private List<Plot> plots;

    @ToString.Exclude
    private PlotParameters plotParameters;
}