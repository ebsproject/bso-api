package org.ebs.service.to;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
@NoArgsConstructor @AllArgsConstructor
public class PlotParameters {

    private Float plotLength;
    private String plotLengthUnit;
    private Float plotWidth;
    private String plotWidthUnit;
    private Float alleyLength;
    private String alleyLengthUnit;
    private String plantingSurfaceCode;
    private Integer rowsPerPlot;

 }