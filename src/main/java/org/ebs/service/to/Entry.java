package org.ebs.service.to;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
@NoArgsConstructor @AllArgsConstructor
public class Entry {

    @JsonProperty("entryId")
    private Integer id;

    @JsonProperty("entryNumber")
    private Integer number;

    private Integer germplasmId;

    private String germplasmName;

    private String parentage;

    private Integer seedId;

    @JsonProperty("entryType")
    private String type;

    @JsonProperty("entryRole")
    private String role;
}