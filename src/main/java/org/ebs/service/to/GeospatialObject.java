package org.ebs.service.to;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRawValue;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
@NoArgsConstructor @AllArgsConstructor
public class GeospatialObject {

    @JsonProperty("geospatialObjectId")
    private int id;
    @JsonProperty("parentGeospatialObjectId")
    private Integer parentId;
    private String code;
    private String name;
    private String type;

    @JsonRawValue
    private String geojson;
}