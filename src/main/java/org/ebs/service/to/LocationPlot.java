package org.ebs.service.to;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
@NoArgsConstructor @AllArgsConstructor
public class LocationPlot {
    private Integer plotId;
    private Integer geId;
    private String locationPlotType;
    private String locationPlotComment;
    private Integer paX;
    private Integer paY;
    private Integer fieldX;
    private Integer fieldY;
    private String barcode;
}