package org.ebs.service.converter;

import static org.ebs.util.Utils.copyNotNulls;

import org.ebs.model.GeospatialObjectModel;
import org.ebs.service.to.GeospatialObject;
import org.ebs.util.Utils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
class GeospatialObjectModelToGeospatialObjectConverter implements Converter<GeospatialObjectModel, GeospatialObject> {

    @Override
    public GeospatialObject convert(GeospatialObjectModel source) {
        GeospatialObject target = new GeospatialObject();
        copyNotNulls(source, target);

        target.setGeojson(geojsonForCoordinates(source.getCoordinates()));
        return target;
    }

    String geojsonForCoordinates(String coordinates) {
        if (coordinates == null) return null;

        return Utils.rawGeojsonFromCoordinates(coordinates);
    }

}
