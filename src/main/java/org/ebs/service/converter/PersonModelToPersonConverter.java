package org.ebs.service.converter;

import static org.ebs.util.Utils.copyNotNulls;

import org.ebs.model.PersonModel;
import org.ebs.service.to.Person;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
class PersonModelToPersonConverter implements Converter<PersonModel, Person> {

    @Override
    public Person convert(PersonModel source) {
        Person target = new Person();
        copyNotNulls(source, target);

        return target;
    }

}