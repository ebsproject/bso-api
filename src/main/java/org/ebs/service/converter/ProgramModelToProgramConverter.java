package org.ebs.service.converter;

import static org.ebs.util.Utils.copyNotNulls;

import org.ebs.model.ProgramModel;
import org.ebs.service.to.Program;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
class ProgramModelToProgramConverter implements Converter<ProgramModel, Program> {

    @Override
    public Program convert(ProgramModel source) {
        Program target = new Program();
        copyNotNulls(source, target);

        return target;
    }

}