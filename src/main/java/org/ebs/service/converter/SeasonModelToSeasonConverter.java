package org.ebs.service.converter;

import static org.ebs.util.Utils.copyNotNulls;

import org.ebs.model.SeasonModel;
import org.ebs.service.to.Season;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
class SeasonModelToSeasonConverter implements Converter<SeasonModel, Season> {

    @Override
    public Season convert(SeasonModel source) {
        Season target = new Season();
        copyNotNulls(source, target);

        return target;
    }

}