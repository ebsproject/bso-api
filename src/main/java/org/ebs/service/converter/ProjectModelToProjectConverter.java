package org.ebs.service.converter;

import static org.ebs.util.Utils.copyNotNulls;

import org.ebs.model.ProjectModel;
import org.ebs.service.to.Project;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
class ProjectModelToProjectConverter implements Converter<ProjectModel, Project> {

    @Override
    public Project convert(ProjectModel source) {
        Project target = new Project();
        copyNotNulls(source, target);

        return target;
    }

}