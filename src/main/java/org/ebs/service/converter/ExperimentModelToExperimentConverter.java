package org.ebs.service.converter;

import static java.util.Collections.emptyList;
import static java.util.Optional.ofNullable;
import static org.ebs.util.Utils.copyNotNulls;

import java.util.List;
import java.util.stream.Collectors;

import org.ebs.model.ExperimentModel;
import org.ebs.model.repository.OccurrenceRepository;
import org.ebs.service.to.Experiment;
import org.ebs.service.to.Occurrence;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
class ExperimentModelToExperimentConverter implements Converter<ExperimentModel, Experiment> {

    private final OccurrenceModelToOccurrenceConverter occConverter;
    private final OccurrenceRepository occurrenceRepo;

    @Override
    public Experiment convert(ExperimentModel source) {
        Experiment target = new Experiment();
        copyNotNulls(source, target);
        target.setStage(source.getStage().getName());
        target.setOccurrences(convertOccurrences(source));

        return target;
    }

    List<Occurrence> convertOccurrences(ExperimentModel source) {
        return ofNullable(occurrenceRepo.findByExperimentIdAndDeletedIsFalse(source.getId()))
                .map(l -> l.stream()
                    .map(o -> occConverter.convert(o))
                    .collect(Collectors.toList()))
                .orElse(emptyList());
    }
}