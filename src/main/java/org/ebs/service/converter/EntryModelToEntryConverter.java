package org.ebs.service.converter;

import static org.ebs.util.Utils.copyNotNulls;

import org.ebs.model.EntryModel;
import org.ebs.service.to.Entry;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
class EntryModelToEntryConverter implements Converter<EntryModel, Entry> {

    @Override
    public Entry convert(EntryModel source) {
        Entry target = new Entry();
        copyNotNulls(source, target);
        target.setGermplasmId(source.getGermplasm().getId());
        target.setGermplasmName(source.getGermplasm().getDesignation());
        String parentage = source.getGermplasm().getParentage().replace("\\", "");
        target.setParentage(null);

        return target;
    }

}