package org.ebs.service.converter;

import static java.util.Collections.emptyList;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;
import static org.ebs.util.Utils.copyNotNulls;

import java.util.List;
import java.util.Optional;

import org.ebs.model.OccurrenceModel;
import org.ebs.model.ScaleModel;
import org.ebs.model.VariableModel;
import org.ebs.model.repository.ExperimentDataRepository;
import org.ebs.model.repository.PlotRepository;
import org.ebs.model.repository.VariableRepository;
import org.ebs.service.to.Occurrence;
import org.ebs.service.to.Plot;
import org.ebs.service.to.PlotParameters;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
class OccurrenceModelToOccurrenceConverter implements Converter<OccurrenceModel, Occurrence> {

    private final PlotModelToPlotConverter plotConverter;
    private final ExperimentDataRepository plotParametersRepo;
    private final VariableRepository variableRepo;
    private final PlotRepository plotRepo;

    @Override
    public Occurrence convert(OccurrenceModel source) {
        Occurrence target = new Occurrence();
        copyNotNulls(source, target);

        target.setLocationId(source.getLocationOccurrenceGroup() == null ? null
                : source.getLocationOccurrenceGroup().getLocation().getId());

        target.setPlantingAreaId(source.getLocationOccurrenceGroup() == null ? null
                : source.getLocationOccurrenceGroup().getLocation().getGeoSpatialObjectId());
        target.setFieldId(null);
        target.setPlots(convertPlots(source));
        target.setRemarks(source.getDescription());
        target.setPlotParameters(convertPlotParameters(source));

        return target;
    }

    List<Plot> convertPlots(OccurrenceModel source) {
        return ofNullable(plotRepo.findByOccurrenceIdAndDeletedIsFalse(source.getId()))
                .map(l -> l.stream().map(p -> plotConverter.convert(p)).collect(toList())).orElse(emptyList());
    }

    PlotParameters convertPlotParameters(OccurrenceModel source) {
        PlotParameters plotParameters = new PlotParameters();

        getPlotParameter(source, "PLOT_LN").ifPresent(p -> {
            plotParameters.setPlotLength(Float.parseFloat(p));
            plotParameters.setPlotLengthUnit(getUnit("PLOT_LN"));
        });
        getPlotParameter(source, "PLOT_LN_1").ifPresent(p -> {
            plotParameters.setPlotLength(Float.parseFloat(p));
            plotParameters.setPlotLengthUnit(getUnit("PLOT_LN_1"));
        });

        getPlotParameter(source, "PLOT_WIDTH").ifPresent(p ->{
            plotParameters.setPlotWidth(Float.parseFloat(p));
            plotParameters.setPlotWidthUnit(getUnit("PLOT_WIDTH"));
        });
        getPlotParameter(source, "PLOT_WIDTH_MAIZE_BED").ifPresent(p ->{
            plotParameters.setPlotWidth(Float.parseFloat(p));
            plotParameters.setPlotWidthUnit(getUnit("PLOT_WIDTH_MAIZE_BED"));
        });
        getPlotParameter(source, "PLOT_WIDTH_RICE").ifPresent(p ->{
            plotParameters.setPlotWidth(Float.parseFloat(p));
            plotParameters.setPlotWidthUnit(getUnit("PLOT_WIDTH_RICE"));
        });
        getPlotParameter(source, "PLOT_WIDTH_WHEAT_FLAT").ifPresent(p ->{
            plotParameters.setPlotWidth(Float.parseFloat(p));
            plotParameters.setPlotWidthUnit(getUnit("PLOT_WIDTH_WHEAT_FLAT"));
        });

        getPlotParameter(source, "ALLEY_LENGTH").ifPresent(p -> plotParameters.setAlleyLength(Float.parseFloat(p)));
        getPlotParameter(source, "ROWS_PER_PLOT_CONT").ifPresent(p -> plotParameters.setRowsPerPlot(Integer.parseInt(p)));

        String alleyLengthUnit = getUnit("ALLEY_LENGTH");
        plotParameters.setAlleyLengthUnit(alleyLengthUnit);

        plotParameters.setPlantingSurfaceCode(null);

        return plotParameters;
    }

    Optional<String> getPlotParameter(OccurrenceModel source, String variableAbbrev) {

        return Optional.ofNullable(plotParametersRepo
                .findByExperimentIdAndVariableAbbrevAndDeletedIsFalse(source.getExperiment().getId(), variableAbbrev)
                .stream().findFirst().map(d -> d.getDataValue()).orElse(null));

    }

    String getUnit(String variableAbbrev) {

        return variableRepo.findByAbbrevAndDeletedIsFalse(variableAbbrev).stream().findFirst()
                .map(VariableModel::getScale).map(ScaleModel::getUnit).orElse(null);

    }
}