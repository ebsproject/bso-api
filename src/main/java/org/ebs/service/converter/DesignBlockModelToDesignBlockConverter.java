
package org.ebs.service.converter;

import static org.ebs.util.Utils.copyNotNulls;

import org.ebs.model.DesignBlockModel;
import org.ebs.service.to.DesignBlock;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
class DesignBlockModelToDesignBlockConverter implements Converter<DesignBlockModel, DesignBlock> {

    @Override
    public DesignBlock convert(DesignBlockModel source) {
        DesignBlock target = new DesignBlock();
        copyNotNulls(source, target);
        return target;
    }
    
}