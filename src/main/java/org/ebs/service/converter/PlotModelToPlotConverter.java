package org.ebs.service.converter;

import java.util.List;

import static java.util.Collections.emptyList;
import static java.util.Optional.ofNullable;
import static org.ebs.util.Utils.copyNotNulls;

import static java.util.stream.Collectors.toList;
import java.util.stream.Collectors;

import org.ebs.model.PlotModel;
import org.ebs.service.converter.DesignBlockModelToDesignBlockConverter;
import org.ebs.service.to.Plot;
import org.ebs.service.to.DesignBlock;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
class PlotModelToPlotConverter implements Converter<PlotModel, Plot> {

    private final DesignBlockModelToDesignBlockConverter designBlockConverter;

    @Override
    public Plot convert(PlotModel source) {
        Plot target = new Plot();
        target.setEntryId(source.getEntry().getId());
        copyNotNulls(source, target);
        target.setDesignBlocks(convertDesignBlock(source));
        return target;
    }

    List<DesignBlock> convertDesignBlock(PlotModel source) {
        return ofNullable(source.getDesignBlocks())
                .map(l -> l.stream().map(o -> designBlockConverter.convert(o)).collect(Collectors.toList()))
                .orElse(emptyList());
    }
}