package org.ebs.service.converter;

import static org.ebs.util.RequestCacheAdvice.REQUEST_CACHE;

import org.ebs.model.EntryModel;
import org.ebs.model.ExperimentModel;
import org.ebs.model.GermplasmModel;
import org.ebs.model.OccurrenceModel;
import org.ebs.model.PlotModel;
import org.ebs.model.ProgramModel;
import org.ebs.model.SeasonModel;
import org.ebs.model.repository.ProgramRepository;
import org.ebs.model.repository.SeasonRepository;
import org.ebs.service.to.PlotInfo;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class PlotModelToPlotInfoConverter implements Converter<PlotModel,PlotInfo> {
    
    private static final String PROGRAM = "PlotModelToPlotInfoConverter.program";
    private static final String SEASON = "PlotModelToPlotInfoConverter.season";
    private static final String LOCATION = "PlotModelToPlotInfoConverter.location";
    
    private final ProgramRepository programRepository;
    private final SeasonRepository seasonRepository;
    
    public PlotInfo convert(PlotModel source) {
        EntryModel entry = source.getEntry();
        ExperimentModel expt = source.getOccurrence().getExperiment();
        GermplasmModel germplasm = entry.getGermplasm();
        
        PlotInfo plotInfo = new PlotInfo(source.getId()
        ,source.getCode()
        ,entry.getCode()
        ,getProgram(expt.getProgramId())
        ,source.getOccurrence().getName()
        ,source.getNumber()
        ,expt.getYear()
        ,getSeason(expt.getSeasonId())
        ,entry.getNumber()
        ,germplasm.getDesignation()
        ,germplasm.getParentage()
        ,getLocation(source.getOccurrence())
        );
        
        return plotInfo;
    }
    
    
    String getProgram(int programId) {
        REQUEST_CACHE.get().computeIfAbsent(PROGRAM,k -> programRepository.findByIdAndDeletedIsFalse(programId)
            .map(ProgramModel::getCode).get());
                
        return REQUEST_CACHE.get().get(PROGRAM).toString();
    }
    String getSeason(int seasonId) {
        REQUEST_CACHE.get().computeIfAbsent(SEASON,k -> seasonRepository.findByIdAndDeletedIsFalse(seasonId)
            .map(SeasonModel::getSeasonCode).get());
                
        return REQUEST_CACHE.get().get(SEASON).toString();
    }

    String getLocation(OccurrenceModel occ) {
        REQUEST_CACHE.get().computeIfAbsent(LOCATION,k -> occ.getLocationOccurrenceGroup().getLocation().getLocationName());
                
        return REQUEST_CACHE.get().get(LOCATION).toString();
    }
}
