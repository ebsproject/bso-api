FROM gradle:6.8.3-jdk8 AS gradleBuild

WORKDIR /home/gradle/project

COPY --chown=gradle:gradle . /home/gradle/project

RUN gradle build --no-daemon --stacktrace

FROM openjdk:11-jre

# PROFILES: prod=Production dev=Development
ENV spring.profiles.active=prod
ENV spring.datasource.url=jdbc:postgresql://localhost:5432/db
ENV spring.datasource.username=postgres
ENV spring.datasource.password=postgres

COPY --from=gradleBuild /home/gradle/project/build/libs/ebs-sg-cb.jar ./app.jar

ENTRYPOINT ["java","-jar","/app.jar"]
